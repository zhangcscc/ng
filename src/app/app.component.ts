import { Component } from '@angular/core';
import { RouterOutlet, Router } from '@angular/router'
import { slideInAnimation } from './animations'

/**
 * 装饰器
 */
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.sass'],
  animations: [ slideInAnimation ]
})

/**
 * 声明了一个类
 */
export class AppComponent {
  title: string = 'angular-demo';
  
  // 传入路由，路由中获取data自定义的animation属性的值作为专场动画名称。
  getAnimationData(outlet: RouterOutlet) { 
    return outlet && outlet.activatedRouteData && outlet.activatedRouteData['animation'];
  }
  constructor(
    private router: Router
  ) {
    console.log(router.config)
  }
}


