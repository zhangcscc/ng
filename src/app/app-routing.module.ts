import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { SelectivePreloadingStrategyService } from './selective-preloading-strategy.service'

/**
 * https://gitee.com/JXHuang_admin/angluar-router-demo
 */
import {AppComponent as APP} from './app.component'
import {AppComponents as home} from './views/home'
import {AppComponents as about} from './views/about'
import { PageNotFoundComponent } from './views/page-not-found.component'

const routes: Routes = [
  {
    path: '', redirectTo: 'home', pathMatch: 'full'
  },
  {
    path: 'home', 
    component: home
  },
  {
    path: 'about', 
    component: about
  },
  {
    path: '**', component: PageNotFoundComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {
    enableTracing: true,
    useHash: false, // 使用hash策略
  })],
  exports: [RouterModule],
  providers:[SelectivePreloadingStrategyService]
})

export class AppRoutingModule { }
