import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import {Router} from '@angular/router'

import { AppRoutingModule } from './app-routing.module';    // 路由
import { AppComponent } from './app.component';             // 根组件
import { BrowserAnimationsModule } from '@angular/platform-browser/animations'  // 入场动画

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { 
  // 审查路由配置
  constructor(router: Router) {
    const replacer = (key: any, value: any) => (typeof value == 'function') ? value.name : value
  }
}
