import { Component } from '@angular/core';

/**
 * 装饰器
 */
@Component({
  selector: 'app-about',
  template: '<h1>关于</h1>',
})

/**
 * 声明了一个类
 */
export class AppComponents {
  title:string = 'angular-demo';
}


