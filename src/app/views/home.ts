import { Component } from '@angular/core';

/**
 * 装饰器
 */
@Component({
  selector: 'app-home',
  templateUrl: './app.home.html',
})

/**
 * 声明了一个类
 */
export class AppComponents {
  title:string = 'angular-demo';
  showTitle: boolean = true;

  // 方法
  fn(event?: Event): void {
    console.log(this);
    this.showTitle = !this.showTitle;
    Object(event?.target).style.cursor = 'pointer';
    
    this.title = this.showTitle ? 'angular-demo' : 'Engine';
    Object(event?.target).parentNode.style.backgroundColor = this.showTitle ? '#F7365F' : '#1976D2';
    
  }
}


