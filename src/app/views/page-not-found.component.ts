import { Component, OnInit } from '@angular/core';

/**
 * 装饰器
 */
@Component({
  selector: 'app-page-not-found',
  template: '<h1>app-page-not-found</h1>',
})

/**
 * 声明了一个类
 */
export class PageNotFoundComponent implements OnInit {
  title:string = 'angular-demo';
  constructor() { }

  ngOnInit(): void {
  }
}


