/*
 * @Descripttion: 自定义预加载策略
 * @Author: junjiaHuang
 * @Date: 2020-07-10 13:56:08
 * @LastEditors: junjiaHuang
 * @LastEditTime: 2020-07-10 14:11:09
 */ 
import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import {Route, PreloadingStrategy } from '@angular/router'
@Injectable({
  providedIn: 'root'
})
/* 
路由器会用两个参数来调用 preload() 方法：
  要加载的路由。
  一个加载器（loader）函数，它能异步加载带路由的模块。
*/
export class SelectivePreloadingStrategyService implements PreloadingStrategy{
  preloadModules: string[] = []
  constructor() { }
  preload(route: Route, load: () => Observable<any>): Observable<any> {
    if(route.data && route.data['preload']) {
      this.preloadModules.push(route.path + '')
      console.log('Preload:' + route.path)
      return load()
    }else {
      return of(null)
    }
  }
}
